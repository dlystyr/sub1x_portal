<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('user_id');
            $table->string('name');
            $table->integer('posts');
            $table->string('rank');
            $table->timestamp('created')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->string('telegram');
            $table->string('discord');
            $table->string('icq');
            $table->string('email');
            $table->string('website');
            $table->boolean('online');
            $table->string('gender');
            $table->integer('age');
            $table->string('location');
            $table->string('gravatar_hash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
