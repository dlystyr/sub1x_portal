<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMasternodeMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_masternode_mapping', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('user_id');
            $table->string('masternode_address');
            $table->longText('signed_message');
            $table->longText('signed_message_hash');
            $table->boolean('valid');
            $table->timestamp('validated')->nullable();
            $table->timestamp('last_change')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_masternode_mapping');
    }
}
